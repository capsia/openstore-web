# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openstore-web package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: openstore-web 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-28 00:36-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/views/Package.vue:153
msgid ""
"*This app contains NSFW content, to view the description and\n"
"  screenshots, click the above button."
msgstr ""

#: src/views/ManagePackage.vue:755
msgid "%{name} was successfully deleted"
msgstr ""

#: src/views/ManagePackage.vue:235
msgid "A comma separated list of keywords"
msgstr ""

#: src/App.vue:40 src/views/About.vue:3 src/views/About.vue:191
#: src/views/About.vue:195
msgid "About"
msgstr ""

#: src/App.vue:78
msgid "About Us"
msgstr ""

#: src/views/Package.vue:343
msgid "Accounts"
msgstr ""

#: src/views/ManageRevisions.vue:62
msgid "Add Another Architecture"
msgstr ""

#: src/views/ManagePackage.vue:86
msgid "Admin"
msgstr ""

#: src/views/Browse.vue:22
msgid "All Categories"
msgstr ""

#: src/views/Browse.vue:35
msgid "All Types"
msgstr ""

#: src/views/Login.vue:26
msgid "An account is not needed to download or install apps."
msgstr ""

#: src/views/ManagePackage.vue:611 src/views/ManagePackage.vue:624
#: src/views/ManageRevisions.vue:175
msgid "An error occured loading your app data"
msgstr ""

#: src/views/ManageRevisions.vue:229 src/views/Submit.vue:222
msgid "An unknown error has occured"
msgstr ""

#: src/views/About.vue:167
msgid "And many amazing developers, testers, and users from the OpenStore community!"
msgstr ""

#: src/views/About.vue:125
msgid ""
"Any personal data you decide to give us (e.g. your email address when registering for an account to upload your apps)\n"
"  goes no further than us,\n"
"  and will not be used for anything other than allowing you to maintain your apps and, in rare cases,\n"
"  contact you in the event there is an issue with your app.\n"
"  Additionally, this site uses cookies to keep you logged in to your account."
msgstr ""

#: src/views/Manage.vue:13
msgid "API Key"
msgstr ""

#: src/components/Types.vue:3 src/views/Manage.vue:53
#: src/views/ManagePackage.vue:384 src/views/Stats.vue:98
msgid "App"
msgstr ""

#: src/views/Submit.vue:101
msgid "App containing or promoting any of the above are strictly prohibited."
msgstr ""

#: src/views/Submit.vue:138
msgid "App Name"
msgstr ""

#: src/App.vue:89
msgid "App Stats"
msgstr ""

#: src/views/Submit.vue:149
msgid "App Title"
msgstr ""

#: src/views/Stats.vue:21
msgid "App Type"
msgstr ""

#: src/views/Stats.vue:17
msgid "App Types"
msgstr ""

#: src/views/Browse.vue:36
msgid "Apps"
msgstr ""

#: src/views/Submit.vue:105
msgid ""
"Apps containing gambling involving real world money transactions are not allowed.\n"
"This includes, but is not limited to, online casinos, betting, and lotteries."
msgstr ""

#: src/views/Submit.vue:88
msgid "Apps containing or promoting child abuse or child sexual abuse are strictly prohibited."
msgstr ""

#: src/views/Submit.vue:94
msgid ""
"Apps containing or promoting gratuitous violence are not allowed.\n"
"This includes, but is not limited to, violence, terrorism, bomb/weapon making, self harm, and grotesque imagery."
msgstr ""

#: src/views/Submit.vue:81
msgid ""
"Apps containing or promoting sexually explicit content are not allowed.\n"
"This includes, but is not limited to, pornography and services promoting sexually explicit content."
msgstr ""

#: src/views/Submit.vue:7
msgid "Apps not requiring manual review can be submitted by logging in with the \"Log In\" link above."
msgstr ""

#: src/views/Submit.vue:112
msgid "Apps promoting illegal activities (based on USA law) are not allowed."
msgstr ""

#: src/views/Submit.vue:20
msgid "Apps requiring manual review must be submitted by contacting an admin in our telegram group."
msgstr ""

#: src/views/Submit.vue:116
msgid ""
"Apps that are found to be stealing the users data, trying to escalate their privileges without user consent,\n"
"or executing malicious process are strictly prohibited."
msgstr ""

#: src/views/About.vue:133
msgid ""
"Apps uploaded to the OpenStore are subject to our submission rules and content policy.\n"
"  You can review the rules and policies before uploading your app."
msgstr ""

#: src/views/ManagePackage.vue:454
msgid "Arch"
msgstr ""

#: src/views/ManagePackage.vue:408 src/views/Package.vue:265
msgid "Architecture"
msgstr ""

#: src/views/Package.vue:344
msgid "Audio"
msgstr ""

#: src/views/Manage.vue:54 src/views/ManagePackage.vue:404
#: src/views/Package.vue:217
msgid "Author"
msgstr ""

#: src/views/Package.vue:6
msgid "Back"
msgstr ""

#: src/views/Badge.vue:28 src/views/Badge.vue:32
#: src/views/ManagePackage.vue:106
msgid "Badge"
msgstr ""

#: src/views/About.vue:18
msgid "Badges"
msgstr ""

#: src/views/Package.vue:345
msgid "Bluetooth"
msgstr ""

#: src/App.vue:37
msgid "Browse"
msgstr ""

#: src/views/Browse.vue:141
msgid "Browse Apps"
msgstr ""

#: src/views/About.vue:77
msgid "Bug Tracker"
msgstr ""

#: src/views/Package.vue:346
msgid "Calendar"
msgstr ""

#: src/views/Package.vue:347
msgid "Camera"
msgstr ""

#: src/views/ManagePackage.vue:515 src/views/ManageRevisions.vue:92
#: src/views/Submit.vue:164
msgid "Cancel"
msgstr ""

#: src/views/Browse.vue:20 src/views/ManagePackage.vue:208
#: src/views/Package.vue:245
msgid "Category"
msgstr ""

#: src/views/ManagePackage.vue:154 src/views/ManageRevisions.vue:69
#: src/views/Package.vue:163
msgid "Changelog"
msgstr ""

#: src/views/ManagePackage.vue:453 src/views/ManageRevisions.vue:4
msgid "Channel"
msgstr ""

#: src/views/ManagePackage.vue:420
msgid "Channels"
msgstr ""

#: src/views/Submit.vue:87
msgid "Child Endangerment"
msgstr ""

#: src/views/ManagePackage.vue:216
msgid "Choose a category"
msgstr ""

#: src/views/ManagePackage.vue:263
msgid "Choose a license"
msgstr ""

#: src/views/ManagePackage.vue:371
msgid "Choose a maintainer"
msgstr ""

#: src/views/Package.vue:348
msgid "Connectivity"
msgstr ""

#: src/views/About.vue:62
msgid "Contact"
msgstr ""

#: src/views/Package.vue:349
msgid "Contacts"
msgstr ""

#: src/views/Package.vue:351
msgid "Content Exchange"
msgstr ""

#: src/views/Package.vue:350
msgid "Content Exchange Source"
msgstr ""

#: src/views/About.vue:142
msgid "Contributors"
msgstr ""

#: src/components/CopyLine.vue:9
msgid "Copy to clipboard"
msgstr ""

#: src/views/Stats.vue:22
msgid "Count"
msgstr ""

#: src/views/ManageRevisions.vue:84
msgid "Create"
msgstr ""

#: src/views/Package.vue:352
msgid "Debug"
msgstr ""

#: src/views/ManagePackage.vue:511
msgid "Delete"
msgstr ""

#: src/views/ManagePackage.vue:143
msgid "Description"
msgstr ""

#: src/views/ManagePackage.vue:66
msgid "Discovery"
msgstr ""

#: src/views/Package.vue:128
msgid "Donate"
msgstr ""

#: src/views/ManagePackage.vue:320
msgid "Donate URL"
msgstr ""

#: src/views/Package.vue:68 src/views/Package.vue:87
msgid "Download"
msgstr ""

#: src/views/ManagePackage.vue:446
msgid "Download Stats"
msgstr ""

#: src/views/About.vue:44
msgid "Download the OpenStore App"
msgstr ""

#: src/views/Manage.vue:57 src/views/Manage.vue:75
#: src/views/ManagePackage.vue:456
msgid "Downloads"
msgstr ""

#: src/views/Package.vue:201
msgid "Downloads of the latest version"
msgstr ""

#: src/views/ManagePackage.vue:178
msgid "Drag and drop to sort screenshots."
msgstr ""

#: src/views/ManagePackage.vue:12
msgid "Edit"
msgstr ""

#: src/views/ManagePackage.vue:611 src/views/ManagePackage.vue:624
#: src/views/ManagePackage.vue:742 src/views/ManagePackage.vue:773
#: src/views/ManageRevisions.vue:175 src/views/ManageRevisions.vue:239
#: src/views/Submit.vue:231
msgid "Error"
msgstr ""

#: src/views/Feeds.vue:24 src/views/Feeds.vue:28
msgid "Feeds"
msgstr ""

#: src/views/ManageRevisions.vue:22
msgid "File"
msgstr ""

#: src/views/ManageRevisions.vue:40
msgid "File Upload"
msgstr ""

#: src/views/About.vue:64
msgid ""
"For any questions you may have or help you may need just chat with us in our telegram group.\n"
"  You can also submit issues, bugs, and feature requests to our GitLab bug tracker."
msgstr ""

#: src/views/ManagePackage.vue:412 src/views/Package.vue:260
msgid "Framework"
msgstr ""

#: src/views/Package.vue:370
msgid "Full System Access"
msgstr ""

#: src/views/Submit.vue:104
msgid "Gambling"
msgstr ""

#: src/views/About.vue:23
msgid "Get your badge"
msgstr ""

#: src/views/Submit.vue:44
msgid ""
"Give a short explanation why you can’t publish the app without extra privileges.\n"
"  No need to go into details, a one liner like \"needs to run a daemon in the background\" will do.\n"
"  List all the special features you have, if there are more."
msgstr ""

#: src/components/Pagination.vue:46
msgid "Go back a page"
msgstr ""

#: src/components/Pagination.vue:47
msgid "Go to the next page"
msgstr ""

#: src/views/About.vue:89
msgid "GPL v3 License"
msgstr ""

#: src/views/Submit.vue:100
msgid "Harassment, Bullying, or Hate Speech"
msgstr ""

#: src/views/Package.vue:353
msgid "History"
msgstr ""

#: src/views/Manage.vue:51
msgid "Icon"
msgstr ""

#: src/views/ManagePackage.vue:400
msgid "ID"
msgstr ""

#: src/views/Submit.vue:49
msgid ""
"If an application could be published without manual review if it wouldn’t be for that one cool feature,\n"
"  publish a stripped down version!\n"
"  Not everyone will want to install apps with less confinement."
msgstr ""

#: src/views/Submit.vue:111
msgid "Illegal Activities"
msgstr ""

#: src/views/Package.vue:354
msgid "In App Purchases"
msgstr ""

#: src/views/ManagePackage.vue:76 src/views/Package.vue:214
msgid "Info"
msgstr ""

#: src/views/About.vue:27 src/views/Package.vue:49
msgid "Install"
msgstr ""

#: src/views/Package.vue:340
msgid "Install via the OpenStore app"
msgstr ""

#: src/components/Pagination.vue:45
msgid "Jump to the first page"
msgstr ""

#: src/components/Pagination.vue:48
msgid "Jump to the last page"
msgstr ""

#: src/views/Package.vue:355
msgid "Keep Display On"
msgstr ""

#: src/views/Manage.vue:17
msgid "Keep your api key private!"
msgstr ""

#: src/views/ManagePackage.vue:227
msgid "Keywords"
msgstr ""

#: src/App.vue:66 src/components/BadgeSelect.vue:4
msgid "Language"
msgstr ""

#: src/views/Browse.vue:49
msgid "Latest Update"
msgstr ""

#: src/views/About.vue:81 src/views/ManagePackage.vue:255
#: src/views/Package.vue:240
msgid "License"
msgstr ""

#: src/views/Package.vue:356
msgid "Location"
msgstr ""

#: src/views/ManagePackage.vue:349
msgid "Locked"
msgstr ""

#: src/App.vue:43
msgid "Log In"
msgstr ""

#: src/views/Login.vue:24
msgid "Log in to the OpenStore to be able to manage your apps."
msgstr ""

#: src/App.vue:49
msgid "Log Out"
msgstr ""

#: src/views/Login.vue:40 src/views/Login.vue:44
msgid "Login"
msgstr ""

#: src/views/Submit.vue:127
msgid "Login to submit your app"
msgstr ""

#: src/views/Login.vue:7
msgid "Login via GitHub"
msgstr ""

#: src/views/Login.vue:12
msgid "Login via GitLab"
msgstr ""

#: src/views/Login.vue:18
msgid "Login via Ubuntu One"
msgstr ""

#: src/views/ManagePackage.vue:364
msgid "Maintainer"
msgstr ""

#: src/views/Submit.vue:115
msgid "Malware"
msgstr ""

#: src/App.vue:46 src/views/Manage.vue:131 src/views/ManagePackage.vue:543
#: src/views/ManagePackage.vue:545
msgid "Manage"
msgstr ""

#: src/views/Manage.vue:127
msgid "Manage Apps"
msgstr ""

#: src/views/Package.vue:357
msgid "Microphone"
msgstr ""

#: src/views/Package.vue:359
msgid "Music Files"
msgstr ""

#: src/views/Manage.vue:52
msgid "Name"
msgstr ""

#: src/views/About.vue:51
msgid "Navigate to the downloads folder"
msgstr ""

#: src/views/Package.vue:360
msgid "Networking"
msgstr ""

#: src/views/Feeds.vue:6
msgid "New Apps"
msgstr ""

#: src/views/ManagePackage.vue:50 src/views/ManageRevisions.vue:124
#: src/views/ManageRevisions.vue:126
msgid "New Revision"
msgstr ""

#: src/views/ManageRevisions.vue:249 src/views/ManageRevisions.vue:250
msgid "New revision for %{channel} was created!"
msgstr ""

#: src/views/ManageRevisions.vue:8
msgid "New Revision for %{name}"
msgstr ""

#: src/views/ManageRevisions.vue:253 src/views/ManageRevisions.vue:254
msgid "New revisions for %{channel} were created!"
msgstr ""

#: src/views/Browse.vue:47
msgid "Newest"
msgstr ""

#: src/views/ManagePackage.vue:41 src/views/ManagePackage.vue:358
msgid "No"
msgstr ""

#: src/views/Browse.vue:106 src/views/Manage.vue:100
msgid "No apps found."
msgstr ""

#: src/views/Package.vue:47
msgid "No longer available"
msgstr ""

#: src/views/Manage.vue:87 src/views/Manage.vue:92
#: src/views/ManagePackage.vue:383 src/views/Package.vue:189
msgid "None"
msgstr ""

#: src/views/Manage.vue:81
msgid "Not published"
msgstr ""

#: src/views/ManagePackage.vue:239
msgid "NSFW"
msgstr ""

#: src/views/Browse.vue:99
msgid "NSFW content"
msgstr ""

#: src/views/Browse.vue:48
msgid "Oldest"
msgstr ""

#: src/views/Browse.vue:50
msgid "Oldest Update"
msgstr ""

#: src/views/Submit.vue:66
msgid ""
"One of the main goals of the OpenStore is to provide a safe app store for people of all ages.\n"
"To accomplish this goal we do not allow certain types of app to be published.\n"
"Failure to follow the guidelines will result in your app being removed.\n"
"Any account with serious infractions may be subject to termination.\n"
"The OpenStore team thanks you for helping us promote a better enviroment for everyone."
msgstr ""

#: src/views/Submit.vue:54
msgid ""
"Only open source applications allowed for manual review:\n"
"  As the applications might have arbitrary access to the device, every manually reviewed app will get a source code review."
msgstr ""

#: src/views/ManagePackage.vue:339
msgid "Only YouTube videos are supported at this time. Make sure the url is for the embedded video!"
msgstr ""

#: src/views/About.vue:50
msgid "Open the terminal app"
msgstr ""

#: src/views/About.vue:149
msgid "OpenStore API/Server Contributors"
msgstr ""

#: src/views/Submit.vue:64
msgid "OpenStore App Content Policy"
msgstr ""

#: src/views/About.vue:161
msgid "OpenStore App Contributors"
msgstr ""

#: src/views/Badge.vue:3
msgid "OpenStore Badges"
msgstr ""

#: src/views/About.vue:70
msgid "OpenStore group on Telegram"
msgstr ""

#: src/views/About.vue:96
msgid "OpenStore source on GitLab"
msgstr ""

#: src/views/About.vue:155
msgid "OpenStore Website Contributors"
msgstr ""

#: src/views/ManagePackage.vue:381
msgid "Override Type"
msgstr ""

#: src/views/ManagePackage.vue:395
msgid "Package Info"
msgstr ""

#: src/views/Package.vue:224
msgid "Packager/Publisher"
msgstr ""

#: src/views/NotFound.vue:3
msgid "Page Not Found"
msgstr ""

#: src/views/Package.vue:119 src/views/Package.vue:170
msgid "Permissions"
msgstr ""

#: src/views/Package.vue:362
msgid "Picture Files"
msgstr ""

#: src/views/ManagePackage.vue:56
msgid "Presentation"
msgstr ""

#: src/views/Submit.vue:75
msgid "Prohibited Apps"
msgstr ""

#: src/views/About.vue:20
msgid "Promote your app on the OpenStore!"
msgstr ""

#: src/views/Badge.vue:5
msgid ""
"Promote your app on the OpenStore! Add these badges to your website,\n"
"  code repository, social media, or anywhere on the internet. The badges\n"
"  are available in several different languages and in both PNG and SVG\n"
"  formats."
msgstr ""

#: src/views/ManagePackage.vue:24
msgid "Public Link"
msgstr ""

#: src/views/ManagePackage.vue:31
msgid "Publish"
msgstr ""

#: src/views/Manage.vue:80 src/views/Package.vue:255
msgid "Published"
msgstr ""

#: src/views/ManagePackage.vue:434
msgid "Published Date"
msgstr ""

#: src/views/Package.vue:363
msgid "Push Notifications"
msgstr ""

#: src/views/Package.vue:358
msgid "Read Music Files"
msgstr ""

#: src/views/Package.vue:361
msgid "Read Picture Files"
msgstr ""

#: src/views/Package.vue:366
msgid "Read Video Files"
msgstr ""

#: src/views/Browse.vue:44
msgid "Relevance"
msgstr ""

#: src/App.vue:108
msgid "Report an Issue"
msgstr ""

#: src/views/Package.vue:466
msgid "Restricted permission"
msgstr ""

#: src/views/ManagePackage.vue:452
msgid "Revision"
msgstr ""

#: src/App.vue:116 src/views/Feeds.vue:3
msgid "RSS Feeds"
msgstr ""

#: src/views/Submit.vue:4
msgid "Rules for Submission"
msgstr ""

#: src/views/Submit.vue:35
msgid "Rules for Submissions Requiring Manual Review"
msgstr ""

#: src/views/About.vue:53
msgid "Run the command:"
msgstr ""

#: src/views/ManagePackage.vue:502
msgid "Save"
msgstr ""

#: src/views/Package.vue:142
msgid "Screenshots"
msgstr ""

#: src/views/ManagePackage.vue:165
msgid "Screenshots (Limit 5)"
msgstr ""

#: src/views/Browse.vue:9 src/views/Manage.vue:25
msgid "Search"
msgstr ""

#: src/views/Submit.vue:39
msgid "Send us a link to a repository for your app to our telegram group along with some instructions on how to build it."
msgstr ""

#: src/views/Package.vue:364
msgid "Sensors"
msgstr ""

#: src/views/Submit.vue:80
msgid "Sexually Explicit Content"
msgstr ""

#: src/views/ManagePackage.vue:488
msgid "Show All Revisions"
msgstr ""

#: src/views/Package.vue:152
msgid "Show NSFW Content"
msgstr ""

#: src/App.vue:100
msgid "Site Status"
msgstr ""

#: src/views/Browse.vue:42
msgid "Sort By"
msgstr ""

#: src/views/Package.vue:236
msgid "Source"
msgstr ""

#: src/App.vue:92
msgid "Source Code"
msgstr ""

#: src/views/ManagePackage.vue:298
msgid "Source URL"
msgstr ""

#: src/views/ManagePackage.vue:96 src/views/Package.vue:197
#: src/views/Stats.vue:3 src/views/Stats.vue:82 src/views/Stats.vue:86
msgid "Stats"
msgstr ""

#: src/views/Manage.vue:55
msgid "Status"
msgstr ""

#: src/App.vue:34 src/views/Submit.vue:160
msgid "Submit"
msgstr ""

#: src/views/Manage.vue:9 src/views/Submit.vue:134 src/views/Submit.vue:190
#: src/views/Submit.vue:194
msgid "Submit App"
msgstr ""

#: src/views/About.vue:140
msgid "Submit your app"
msgstr ""

#: src/views/ManagePackage.vue:728 src/views/ManagePackage.vue:756
#: src/views/ManageRevisions.vue:258
msgid "Success"
msgstr ""

#: src/views/Package.vue:228
msgid "Support"
msgstr ""

#: src/views/ManagePackage.vue:309
msgid "Support URL"
msgstr ""

#: src/views/ManagePackage.vue:132
msgid "Tag Line"
msgstr ""

#: src/App.vue:81
msgid "Telegram"
msgstr ""

#: src/views/About.vue:105
msgid "Terms"
msgstr ""

#: src/views/Package.vue:10
msgid "The app you are looking for has been removed or does not exist"
msgstr ""

#: src/views/About.vue:100
msgid ""
"The apps available for download from the OpenStore have their own licencing and terms.\n"
"  Consult each individual app's page for more information."
msgstr ""

#: src/views/ManagePackage.vue:727
msgid "The changes to %{name} were saved!"
msgstr ""

#: src/views/About.vue:107
msgid ""
"The OpenStore a non-profit volunteer project.\n"
"  Although every effort is made to ensure that everything in the repository is safe to install, you use it AT YOUR OWN RISK.\n"
"  Apps uploaded to the OpenStore will be subject to an automated review process or\n"
"  a manual review process to check for potential security or privacy issues.\n"
"  This checking is far from exhaustive though, and there are no guarantees."
msgstr ""

#: src/views/About.vue:29
msgid ""
"The OpenStore app is installed by default on the UBports builds of Ubuntu Touch.\n"
"  But if you need to install the OpenStore manually, follow these steps:"
msgstr ""

#: src/views/About.vue:145
msgid "The OpenStore is made possible by many amazing contributors:"
msgstr ""

#: src/views/About.vue:5
msgid ""
"The OpenStore is the official app store for Ubuntu Touch.\n"
"  It is an open source project run by a team of volunteers with help from the community.\n"
"  You can discover and install new apps on your Ubuntu Touch device.\n"
"  You can also upload and manage your own apps for publication.\n"
"  The OpenStore encourages the apps published within to be open source, but also accepts proprietary apps."
msgstr ""

#: src/views/About.vue:115
msgid ""
"The OpenStore respects your privacy.\n"
"  We don't track you or your device.\n"
"  You do not need an account to download and install apps and we do not track your downloads.\n"
"  For developer information we count the number of downloads per app revision.\n"
"  Information about your device (the cpu architecture, framework versions, OS version, and OS language)\n"
"  are sent to the server to allow it to filter apps to only what you can install and present your native language where possible.\n"
"  This information is not logged or stored and cannot be used to uniquely identify you."
msgstr ""

#: src/views/NotFound.vue:5
msgid "The page you are looking for has been removed or does not exist"
msgstr ""

#: src/views/About.vue:83
msgid ""
"The source code for OpenStore related projects is available under the GPL v3.\n"
"  You can find the code on the OpenStore GitLab page."
msgstr ""

#: src/views/Package.vue:16
msgid "There was an error trying to find this app, please refresh and try again."
msgstr ""

#: src/views/Browse.vue:68 src/views/Manage.vue:36
msgid "There was an error trying to load the app list, please refresh and try again."
msgstr ""

#: src/views/Stats.vue:5
msgid "There was an error trying to load the stats, please refresh and try again."
msgstr ""

#: src/views/ManagePackage.vue:247
msgid "This app contains NSFW material"
msgstr ""

#: src/views/Package.vue:480 src/views/Package.vue:481
msgid "This app does not have access to restricted system data, see permissions for more details"
msgstr ""

#: src/views/Package.vue:484 src/views/Package.vue:485
msgid "This app has access to restricted system data, see permissions for more details"
msgstr ""

#: src/views/Submit.vue:140
msgid ""
"This is the unique identifier for your app.\n"
"  It must match exactly the \"name\" field in your click's manifest.json and must be all lowercase letters.\n"
"  For example: \"openstore.openstore-team\", where \"openstore\" is the app and \"openstore-team\"\n"
"  is the group or individual authoring the app."
msgstr ""

#: src/views/ManagePackage.vue:582
msgid "This revision is still available"
msgstr ""

#: src/views/ManageRevisions.vue:77
msgid "This will be added to the beginning of your current changelog"
msgstr ""

#: src/views/ManagePackage.vue:121
msgid "Title"
msgstr ""

#: src/views/Browse.vue:45
msgid "Title (A-Z)"
msgstr ""

#: src/views/Browse.vue:46
msgid "Title (Z-A)"
msgstr ""

#: src/views/ManagePackage.vue:477
msgid "Total"
msgstr ""

#: src/views/Package.vue:206
msgid "Total downloads"
msgstr ""

#: src/views/Manage.vue:85
msgid "Total:"
msgstr ""

#: src/views/ManagePackage.vue:425 src/views/Package.vue:270
msgid "Translation Languages"
msgstr ""

#: src/views/Browse.vue:107 src/views/Manage.vue:101
msgid "Try searching for something else"
msgstr ""

#: src/views/Browse.vue:33 src/views/ManagePackage.vue:416
msgid "Type"
msgstr ""

#: src/views/About.vue:14
msgid "Ubuntu Touch"
msgstr ""

#: src/views/Browse.vue:3
msgid "Ubuntu Touch Apps"
msgstr ""

#: src/views/Stats.vue:96
msgid "Uncategorized"
msgstr ""

#: src/views/Package.vue:184
msgid "Unrestricted read access to:"
msgstr ""

#: src/views/Package.vue:180
msgid "Unrestricted write access to:"
msgstr ""

#: src/views/Package.vue:250
msgid "Updated"
msgstr ""

#: src/views/Feeds.vue:10
msgid "Updated Apps"
msgstr ""

#: src/views/ManagePackage.vue:440
msgid "Updated Date"
msgstr ""

#: src/views/ManageRevisions.vue:13
msgid "Upload Revision via:"
msgstr ""

#: src/views/ManageRevisions.vue:23 src/views/ManageRevisions.vue:50
msgid "URL"
msgstr ""

#: src/views/ManageRevisions.vue:57
msgid "URL of App from the Web"
msgstr ""

#: src/views/Package.vue:365
msgid "User Metrics"
msgstr ""

#: src/views/Manage.vue:56 src/views/ManagePackage.vue:455
msgid "Version"
msgstr ""

#: src/views/Package.vue:368
msgid "Video"
msgstr ""

#: src/views/Package.vue:367
msgid "Video Files"
msgstr ""

#: src/views/ManagePackage.vue:331
msgid "Video URL"
msgstr ""

#: src/views/About.vue:35
msgid "View Install Instructions"
msgstr ""

#: src/views/Submit.vue:93
msgid "Violence"
msgstr ""

#: src/components/Types.vue:4 src/views/ManagePackage.vue:385
#: src/views/Stats.vue:99
msgid "Web App"
msgstr ""

#: src/components/Types.vue:5 src/views/ManagePackage.vue:386
#: src/views/Stats.vue:100
msgid "Web App+"
msgstr ""

#: src/views/Browse.vue:37
msgid "Web Apps"
msgstr ""

#: src/views/Package.vue:369
msgid "Webview"
msgstr ""

#: src/views/Manage.vue:4
msgid "Welcome %{name}!"
msgstr ""

#: src/views/ManageRevisions.vue:8 src/views/ManageRevisions.vue:248
msgid "Xenial"
msgstr ""

#: src/views/ManagePackage.vue:40 src/views/ManagePackage.vue:357
msgid "Yes"
msgstr ""

#: src/views/Submit.vue:11
msgid "You are only allowed to publish apps that you have permission to distribute."
msgstr ""

#: src/views/Submit.vue:25
msgid "You must read over the content policy detailed below."
msgstr ""

#: src/views/Submit.vue:15
msgid ""
"Your app can be pulled without warning at the discretion of our admins.\n"
"  Where possible, we will contact you regarding any such actions."
msgstr ""
